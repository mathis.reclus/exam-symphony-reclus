<?php

namespace App\Entity;

use App\Entity\Tour;
use App\Repository\CompanyRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompanyRepository::class)]
class Company
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToOne(targetEntity: Tour::class, inversedBy: 'companies')]
    private $Tour;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTour(): ?Tour
    {
        return $this->Tour;
    }

    public function setTour(?Tour $Tour): self
    {
        $this->Tour = $Tour;

        return $this;
    }

    public function getAllStops(Tour $tour) {
        $tour->getStops();
        return $tour;

    }

    public function getBestCapacityTours(Tour $tour )
    {
        $repository = $doctrine->getRepository(Product::class);
        return $this->getTours()->findOneby($tour, $orderBy = $tour('capacity'));

    }
}