<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513084114 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, tour_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_4FBF094F15ED8D43 (tour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stop (id INT AUTO_INCREMENT NOT NULL, tour_id INT DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, date DATE DEFAULT NULL, INDEX IDX_B95616B615ED8D43 (tour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tour (id INT AUTO_INCREMENT NOT NULL, main_event VARCHAR(255) DEFAULT NULL, capacity INT DEFAULT NULL, price INT DEFAULT NULL, start_date DATE DEFAULT NULL, stop_date DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F15ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id)');
        $this->addSql('ALTER TABLE stop ADD CONSTRAINT FK_B95616B615ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094F15ED8D43');
        $this->addSql('ALTER TABLE stop DROP FOREIGN KEY FK_B95616B615ED8D43');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE stop');
        $this->addSql('DROP TABLE tour');
    }
}
