<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513091325 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stop_tour (stop_id INT NOT NULL, tour_id INT NOT NULL, INDEX IDX_80373B763902063D (stop_id), INDEX IDX_80373B7615ED8D43 (tour_id), PRIMARY KEY(stop_id, tour_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stop_tour ADD CONSTRAINT FK_80373B763902063D FOREIGN KEY (stop_id) REFERENCES stop (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stop_tour ADD CONSTRAINT FK_80373B7615ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stop DROP FOREIGN KEY FK_B95616B615ED8D43');
        $this->addSql('DROP INDEX IDX_B95616B615ED8D43 ON stop');
        $this->addSql('ALTER TABLE stop DROP tour_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE stop_tour');
        $this->addSql('ALTER TABLE stop ADD tour_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stop ADD CONSTRAINT FK_B95616B615ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id)');
        $this->addSql('CREATE INDEX IDX_B95616B615ED8D43 ON stop (tour_id)');
    }
}
